CC := gcc

SNIF := snif
SNIF_SOURCES := src/snif.c

LIBS := -lpcap -lpthread

.PHONY: all

all:
	$(CC) $(SNIF_SOURCES) -o $(SNIF) $(LIBS)
