#include <mysql.h>
#include <my_global.h>
#include <pcap.h>

MYSQL_RES* db_get_packets(MYSQL* connect, int port, unsigned int count)
{
  char* req = (char*)malloc(80+sizeof(int)*2+1);
  sprintf(req, "SELECT* FROM(SELECT * FROM p%d ORDER BY id\
 DESC LIMIT %u)Var1 ORDER BY Id ASC;", port, count);
  if (mysql_query(connect, req)) 
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
  }
  MYSQL_RES* result = mysql_store_result(connect);
  return result;
}

unsigned int db_get_new_id(MYSQL* connect, int port)
{
  char* req = (char*)malloc(26+sizeof(int)+1);
  sprintf(req, "SELECT max(Id) FROM p%d;", port);
  if (mysql_query(connect, req))
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
  }
  MYSQL_RES *result = mysql_store_result(connect);
  unsigned int num_fields = mysql_num_fields(result);
  MYSQL_ROW row = mysql_fetch_row(result);
  free(req);
  mysql_free_result(result);
  unsigned int id = atoi(row[0]);
  return id;
}

int db_check_exists(MYSQL* connect, char* tname)
{
  char *check = (char*)malloc(100);
  sprintf(check, "SELECT count(*)FROM information_schema.tables WHERE\
 table_schema ='snif'AND table_name ='%s'", tname);
  if (mysql_query(connect, check))
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
  }
  MYSQL_RES *result = mysql_store_result(connect);
  int num_fields = mysql_num_fields(result);
  MYSQL_ROW row = mysql_fetch_row(result);
  free(check);
  mysql_free_result(result);
  return atoi(row[0]);
}

unsigned int db_get_id(MYSQL* connect, int port)
{
  unsigned int id =1;
  return id;
}

void db_add_packet(MYSQL* connect, unsigned int size,
    const char* packet, int port)
{
  char *st = "INSERT INTO p%d(packet) VALUES('%s');";
  char *chunk = (char*)malloc(2*size+1);
  mysql_real_escape_string(connect, chunk, packet, size);
  char *query = (char*)malloc(strlen(st)+2*size+1);
  long int len = snprintf(query, strlen(st)+2*size+1, st, port, chunk);
  if (mysql_real_query(connect, query, len))
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
    mysql_close(connect);
  }
  free(chunk);
  free(query);
}

void db_dump_from(MYSQL* connect, pcap_t *handle,
    const struct pcap_pkthdr *header)
{
  mysql_query(connect, "SELECT * FROM p8000;");
  MYSQL_RES *result = mysql_store_result(connect);
  MYSQL_ROW row = mysql_fetch_row(result);
  pcap_dumper_t *dumpfile = pcap_dump_open(handle, "packet.pcap");
  pcap_dump((unsigned char *)dumpfile, header, row[0]);
  exit(0);
}

void db_create_table(MYSQL* connect, char* tname)
{
  char *db_req = (char*)malloc(128);
  sprintf(db_req, "CREATE TABLE IF NOT EXISTS %s (Id INT\
 NOT NULL AUTO_INCREMENT, packet BLOB, PRIMARY KEY (Id));", tname);
  if (mysql_query(connect, db_req))
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
    mysql_close(connect);
  }
  free(db_req);
}

void db_clear()
{
  MYSQL* connect = mysql_init(NULL);
  char* req = NULL;
  char* dropt = "drop table %s;";

  if (mysql_real_connect(connect, "localhost", "root", "lilolol777",   
    "snif", 0, NULL, 0) == NULL)
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
    mysql_close(connect);
  }
  
  if (mysql_query(connect, "show tables;"))
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
  }
  MYSQL_RES *result = mysql_store_result(connect);
  int num_fields = mysql_num_fields(result);
  MYSQL_ROW row;
  while(row = mysql_fetch_row(result))
  {
    req = (char*)realloc(req, strlen(row[0])+strlen(dropt));
    sprintf(req, dropt, row[0]);
    if (mysql_query(connect, req))
    {
      fprintf(stderr, "%s\n", mysql_error(connect));
    }
  }
  free(req);
  mysql_free_result(result);
  mysql_close(connect);
}

MYSQL* db_connect(const char* db)
{
  MYSQL *connect = mysql_init(NULL);
  if (connect == NULL) 
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
    exit(-1);
  }
  if (mysql_real_connect(connect, "localhost", "root", "lilolol777", 
    db, 0, NULL, 0) == NULL) 
  {
    fprintf(stderr, "%s\n", mysql_error(connect));
    mysql_close(connect);
    exit(-1);
  }
  return connect;
}
