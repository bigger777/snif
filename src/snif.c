#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <pcap.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <getopt.h>

#include "snif.h"

#define PORT 17030
#define DUMP_FILE "./dump/trace-08-59-32"

const char * help = "Usage: snif [options]\n \
options\n \
 -h: print help\n \
 -i: set interface for sniffing\n \
 -f: path to .pcap file for sniffing\n \
 -r: reset date base\n \
 -l: start with default interface\n";

/* Обработка пакетов*/
void got_packet(u_char * data, const struct pcap_pkthdr * header,
    const u_char * packet)
{
  if (NULL == packet)
  {
    return;
  }

  struct ethhdr  * eth_header  = NULL;
  struct iphdr   * ip_header   = NULL;
  struct ipv6hdr * ipv6_header = NULL;
  struct udphdr  * udp_header  = NULL;
  struct tcphdr  * tcp_header  = NULL;
  const char     * payload     = NULL;

  u_int8_t ip_header_len;
  u_int8_t tcp_header_len;
  u_int8_t payload_size;

  eth_header = (struct ethhdr *)(packet);
  ip_header = (struct iphdr *)(packet + ETH_HLEN);
  ip_header_len = IP_HL(ip_header);

  if (ip_header_len < 20)
  {
    printf("Invalid IP header length: %d bytes\n", ip_header_len);
    return;
  }

  switch(ip_header->protocol)
  {
    case IPPROTO_TCP:
      printf("TCP: ");
      tcp_header = (struct tcphdr *)(packet + ETH_HLEN + ip_header_len);
      tcp_header_len = TCP_HL(tcp_header);
      if (tcp_header_len < 20)
      {
        printf("Invalid TCP header length: %u bytes\n", tcp_header_len);
        return;
      }
      payload = (const char *)(packet + ETH_HLEN +
          ip_header_len + tcp_header_len);
      payload_size = ntohs(ip_header->tot_len) -
        (ip_header_len + tcp_header_len);
      break;

    case IPPROTO_UDP:
      printf("UDP: ");
      udp_header = (struct udphdr *)(packet + ETH_HLEN + ip_header_len);
      payload = (const char *)(packet + ETH_HLEN + UDP_HLEN);
      payload_size = ntohs(ip_header->tot_len) - (ip_header_len + UDP_HLEN);
      break;

    default:
      printf("Unknow protocol\n");
      return;
  }

  /*TODO: ПРЕДВАРИТЕЛЬНО СЖИМАТЬ ПАКЕТЫ*/
  printf("Get payload with size %d\n", payload_size);
}

/* Поток вызывает функцию захвата пакетов */
void * capture(void * args)
{
  //struct snif_data* data = (struct snif_data*)s_data;
  pcap_t * handle = (pcap_t *)args;
  pcap_loop(handle, -1, got_packet, NULL);
  pcap_close(handle);
}

/* TODO:
 * 1. Демонизировать процесс захвата пакетов.
 * 2. Выделять TCP сессии и заносить в бд. Определиться со способом хранения
 * сессий.
 */

int main(int argc, char ** argv)
{
  pthread_t snif_tid;
  pcap_t * handle = NULL;
  char errbuf[PCAP_ERRBUF_SIZE];
  char * dev = NULL;
  int rez;
  int opt_index;
  const struct option long_options[] = {
    {"reset", no_argument, NULL, 'r'},
    {"file", required_argument, NULL, 'f'},
    {"iface", required_argument, NULL, 'i'},
    {"help", no_argument, NULL, 'h'},
    {"live", no_argument, NULL, 'l'},
    {NULL, 0, NULL, 0}
  };

/* -r: очистка бд
 * -f: указать файл для сниффинга 
 * -i: указать интерфейс для сниффинга
 * -h: доступные флаги
 */
  if(argc != 1)
  {
    while ((rez = getopt_long(argc, argv, "f:i:rlh", long_options,
            &opt_index)) != -1)
    {
      switch (rez)
      {
        case 'r':
          //db_clear();
          //break;
          return 0;

        case 'h':
          printf("%s", help);
          return 0;

        case 'f':
          if (optarg != NULL)
          {
            handle = pcap_open_offline(optarg, errbuf);
            if (NULL == handle)
            {
              printf("<%s> %s\n", __func__, errbuf);
              return -1;
            }
          }
          else
          {
            printf("Require path to .pcap file\n");
            return -1;
          }
          goto start;

        case 'i':
          handle = pcap_open_live(optarg, BUFSIZ, 1, 1000, errbuf);
          if (NULL == handle)
          {
            printf("<%s> %s\n", __func__, errbuf);
            return -1;
          }
          goto start;

        case 'l':
          dev = pcap_lookupdev(errbuf);
          if (NULL == dev)
          {
            printf("<%s> %s\n", __func__, errbuf);
            return -1;
          }
          handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
          if (NULL == handle)
          {
            printf("<%s> %s\n", __func__, errbuf);
            return -1;
          }
          goto start;

        default:
          return 0;
      }
    }
  }
  else // default start
  {
    handle = pcap_open_offline(DUMP_FILE, errbuf);
    if (NULL == handle)
    {
      printf("<%s> %s", __func__, errbuf);
      return -1;
    }
  }

start:
  /* Содание потока захвата пакетов */
  pthread_create(&snif_tid, NULL, capture, (void *)handle);
  pthread_join(snif_tid, NULL);
  return 0;
}
