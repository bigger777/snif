#ifndef _SNIF_H_
#define _SNIF_H_

#define UDP_HLEN 8

#define IP_HL(ipv4_header) \
  (((ipv4_header)->ihl) * 4)

#define TCP_HL(tcp_header) \
  (((tcp_header)->doff) * 4)

#define UDP_HL(udp_header) \
  (udp_header)->len


#endif // _SNIF_H_
