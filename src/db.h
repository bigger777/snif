#ifndef _DB_H_
#define _DB_H_

#include <mysql.h>                                                              
#include <my_global.h>

int db_check_exists(MYSQL* connect, char* tname);
void db_add_packet(MYSQL* connect, unsigned int size,
    const char* packet, int port);
void db_dump_from(MYSQL* connect, pcap_t *handle,
    const struct pcap_pkthdr *header);
void db_create_table(MYSQL* connect, char* tname);
void db_clear();
MYSQL* db_connect(const char* db);
unsigned int db_get_new_id(MYSQL* connect, int port);
MYSQL_RES* db_get_packets(MYSQL* connect, int port, unsigned int count);

#endif
